import matplotlib.pyplot as pl
import pandas as pd
import scipy.interpolate
from scipy.special import *

import geom_params
import spatial as sp


class AVSP_press_mode(object):
    """
    This class builds a p_mode from a csv file.
    The csv file is made by a python script that takes a csv from a 1D plot dataframe from paraview.
    This python script creates a "basis*.csv" file used to initialize this Mode object.
    """

    def __init__(self, csv_file, name='default_name'):
        self.name = name
        self.csv_file = csv_file
        self.dataframe = None
        self.set_pts = None
        self.pts = None
        self.modulus = {}
        self.phase = {}
        self.real = {}
        self.imag = {}
        self.p_mode = {}

        self.read_csv()
        self.get_set_of_points()
        self.compute_mode()

    def read_csv(self):
        # read row dataframe from .csv
        self.dataframe = pd.read_csv(self.csv_file)

    def get_set_of_points(self):
        """
        Init set of points from pandas dataframe
        :return:
        """
        df = self.dataframe
        list_points = []

        for i in df.index:
            # buffer
            probe_name = df['Probe'].iloc[[i]].values[0]
            probe_x = df['x'].iloc[[i]].values[0]
            probe_y = df['y'].iloc[[i]].values[0]
            probe_z = df['z'].iloc[[i]].values[0]

            # create point@
            pt = sp.Point(name=probe_name, x=probe_x, y=probe_y, z=probe_z)

            # Append point
            list_points.append(pt)

        self.set_pts = sp.Set_of_points(name='Points from %s' % self.csv_file, list_points=list_points)
        self.pts = self.set_pts.points

    def __str__(self):
        """
        Print all attributes
        """
        res = """
-----------------------------------------------------
                        Mode 
-----------------------------------------------------"""
        res += """
        Mode name           =   %s
        set of points name  =   %s
        nb of points in set =   %s
        Norm before norming =   %2.8e""" % (
            self.name, self.set_pts.name, self.set_pts.nb, self.normed_by)
        res += """
-----------------------------------------------------"""
        return res

    def compute_mode(self):
        df = self.dataframe
        df['p_mode'] = df['re_p_mode'] + 1j * df['im_p_mode']
        mode_vec = np.array(df['p_mode'].values)

        print mode_vec

        # Norm of p_mode vector
        mode_vec = np.array(mode_vec)
        norm = np.sqrt(np.vdot(mode_vec, mode_vec))

        self.normed_by = np.real(norm)

        normed_vec = mode_vec / norm

        # Norming the p_mode
        for i, pt in enumerate(self.pts):
            p_mode = mode_vec[i] / self.normed_by

            self.p_mode[pt] = p_mode
            self.modulus[pt] = np.abs(p_mode)
            self.phase[pt] = np.angle(p_mode)
            self.real[pt] = np.real(p_mode)
            self.imag[pt] = np.imag(p_mode)


class CylindricalModeTheo(object):
    """
    Analytic cylindrical p_mode object
    """

    def __init__(self, name='Default_Mode_name', set_of_points=None, n=None, m=None, q=None, P=1, Q=1.,
                 user_frequency_spec=None):
        """
        Description de la fonction et elle fait ca
        :param name: type, dimension k... ,
        :param set_of_points:
        :param n:
        :param m:
        :param q:
        :param P:
        :param Q:
        """
        self.name = name
        self.set_pts = set_of_points
        self.pts = set_of_points.points

        self.modulus = {}
        self.phase = {}
        self.real = {}
        self.imag = {}
        self.mode = {}
        self.max_modulus = 0.

        self.n = n
        self.m = m
        self.q = q

        # Fills modulus, phase, real and imag
        if not set_of_points is None:
            self.compute_mode(n=n, q=q, P=P, Q=Q, beta=get_beta(m=m, n=n))

        print "Frequency of p_mode %s = %f Hz" % (self.name, self.frequency)

        if not user_frequency_spec is None:
            self.frequency = user_frequency_spec

    def print_self(self):
        """
        Print all attributes
        """
        print """
-----------------------------------------------------
                        Mode 
-----------------------------------------------------"""
        print """
        Mode name           =   %s
        set of points name  =   %s
        nb of points in set =   %s
        Mode Frequency      =   %2.2e
        Norm before norming =   %2.2e""" % (
            self.name, self.set_pts.name, self.set_pts.nb, self.frequency, self.normed_by)
        print """
-----------------------------------------------------"""

    def compute_mode(self, n=0., q=0., P=1., Q=1., beta=0.):

        mode_vec = []

        for pt in self.pts:
            r = pt.r
            th = pt.th
            z = pt.z

            if not r > geom_params.a:
                p_r = jn(1, (beta * np.pi * r / geom_params.a))

            else:
                p_r = np.nan
                print "Point %s ==> NaN, r = %0.2e > a = %0.2e" % (pt.name, r, geom_params.a)

            if not z > geom_params.L:
                p_z = np.cos(q * np.pi * z / geom_params.L)

            else:
                p_z = np.nan
                print "Point %s ==> NaN, z = %0.2e > L = %0.2e" % (pt.name, z, geom_params.L)

            # No conditions needed on theta
            p_th = P * np.exp(1j * n * th) + Q * np.exp(-1j * n * th)

            p_mode = p_r * p_z * p_th
            p_complex = (1. + 1j) * p_mode

            # fill the p_mode list in order to norm the p_mode vectors
            mode_vec.append(p_complex)

            # Just a simple warning
            if np.isnan(p_complex):
                print "Point %s ==> NaN" % pt.name

        # Norm of p_mode vector
        mode_vec = np.array(mode_vec)
        norm = np.sqrt(np.vdot(mode_vec, mode_vec))

        self.normed_by = np.real(norm)
        print "normed by == ", self.normed_by
        print "norm == ", self.normed_by

        normed_vec = mode_vec / norm
        norm_of_normed = np.sqrt(np.vdot(normed_vec, normed_vec))

        print "norm of normed vector ", norm_of_normed

        # Norming the p_mode
        for i, pt in enumerate(self.pts):
            p_mode = mode_vec[i] / self.normed_by

            if np.abs(p_mode) > self.max_modulus:
                self.max_modulus = np.abs(p_mode)

            self.mode[pt] = p_mode
            self.modulus[pt] = np.abs(p_mode)
            self.phase[pt] = np.angle(p_mode)
            self.real[pt] = np.real(p_mode)
            self.imag[pt] = np.imag(p_mode)

        self.frequency = (geom_params.c / 2.) * ((beta / geom_params.a) ** 2. + (q / geom_params.L) ** 2.) ** 0.5


# outside of class Mode
def get_beta(m=None, n=None):
    """
    Returns value of beta for orders m, n
    """
    beta = np.array([[0, 1.22, 2.333, 3.238], [0.586, 1.697, 2.717, 3.725], [0.972, 2.135, 3.173, 4.192]])

    print "beta = %f" % beta[n][m]

    return beta[n][m]


def plot_mode(mode, my_m, my_n, nlevels=10., color_map='RdBu'):
    x = []
    y = []
    z = []

    p_modulus = []
    p_phase = []
    p_real = []
    p_imag = []

    for pt in mode.pts:
        x.append(pt.x)
        y.append(pt.y)
        z.append(pt.z)

        p_modulus.append(mode.modulus[pt])
        p_phase.append(mode.phase[pt])
        p_real.append(mode.real[pt])
        p_imag.append(mode.imag[pt])

    p_modulus = np.array(p_modulus)
    p_phase = np.array(p_phase)
    p_real = np.array(p_real)
    p_imag = np.array(p_imag)

    X, Y = np.meshgrid(x, y, sparse=False)

    if True:
        Z = np.diag(x)
        Z = X

        data_for_2D_plot = p_modulus * np.cos(p_phase)
        data_for_2D_plot /= np.amax(data_for_2D_plot)
        # data_for_2D_plot = p_real
        print "amax pressure field = %8.8e" % np.amax(data_for_2D_plot)

        refinement = 3.

        pl.figure(figsize=(5, 5))

        for factor in [refinement]:
            for met in ['cubic']:
                X_grid, Y_grid, grid_interp = interpolate_on_grid(int(factor * len(x)), x, y, data_for_2D_plot,
                                                                  method=met)

                print "shape grid_interp", grid_interp.shape

                g_max = np.nanmax(grid_interp)
                g_min = np.nanmin(grid_interp)

                nlevels = 301
                levels = np.linspace(g_min, g_max, nlevels)
                ctr_f = pl.contourf(X_grid, Y_grid, grid_interp, levels)  # , cmap=color_map)

                nlevels_ctr = 12
                levels_ctr = np.linspace(g_min, g_max, nlevels_ctr, endpoint=True)
                ctr = pl.contour(X_grid, Y_grid, grid_interp, levels_ctr, colors='black', linewidth=.25)
                # pl.clim(-1, 1)
                # pl.colorbar()

                frameon = False
                # pl.xlabel('x')
                # pl.ylabel('y')

                pl.axis('equal')
                pl.xlim((-1., 1.))
                pl.ylim((-1., 1.))
                # pl.gca().yaxis.set_major_formatter(mtick.FormatStrFormatter('%.1f'))
                # pl.gca().xaxis.set_major_formatter(mtick.FormatStrFormatter('%.1f'))
                # pl.locator_params(nticks=5)
                # pl.locator_params(axis='y', nticks=6)
                # pl.locator_params(axis='x', nticks=10)
                pl.xticks(np.linspace(-1., 1., 5))
                pl.yticks(np.linspace(-1., 1., 5))

                n = int(factor * len(x))
                # pl.colorbar(ctr_f)
                # xticks([]), yticks([])
                pl.tight_layout()
                # pl.savefig('./Figures/0000_Mode_' + p_mode.name + '_grid_interp_%s_%s' % (met, n) + '.png',
                #            dpi=200)
                pl.savefig('./0000_Mode_%sR_%sT' % (my_n, my_m) + '.png', dpi=200)
                # pl.close()
                # pl.show()

    return None


def interpolate_on_grid(dx, x, y, data_to_interpolate, xx=None, yy=None, method=None):
    nx, ny = (dx, dx)

    x_min = np.amin(x)
    y_min = np.amin(y)

    x_max = np.amax(x)
    y_max = np.amax(y)

    if (xx == None) or (yy == None):
        xx = np.linspace(x_min, x_max, nx)
        yy = np.linspace(y_min, y_max, ny)

        X_grid, Y_grid = np.meshgrid(xx, yy)
    else:
        X_grid, Y_grid = xx, yy

    grid_interp = scipy.interpolate.griddata((x, y), data_to_interpolate.flatten(), (X_grid, Y_grid), method=method)

    return X_grid, Y_grid, grid_interp
