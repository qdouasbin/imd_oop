import numpy as np
import matplotlib.pyplot as pl

import spatial as sp
import Modes as md
import Signals as sig
import Projection as pj

# from geom_params import *

# ------------- Parameters --------------
lst_r = [0.7]
lst_th = np.array([0.75]) * np.pi
lst_z = [0.]
# Creating temporal array
n_per = 2
pt_per = 50
# ------------- Parameters --------------


list_points_test = []

for i_r, r in enumerate(lst_r):
    for i_th, th in enumerate(lst_th):
        for i_z, zz in enumerate(lst_z):
            pt_name = 'r%s_th%s_z%s' % (i_r, i_th, i_z)
            list_points_test.append(sp.Point(name=pt_name, x=r * np.cos(th), y=r * np.sin(th), z=zz))

# Creating set of points
Sop = sp.Set_of_points(name='set_for_plot_test', list_points=list_points_test)

# Creating modes
mode_1T = md.CylindricalModeTheo(name='1T', set_of_points=Sop, m=1, n=1, q=0., user_frequency_spec=2.)

# Temporal array with _pt_per_ points per periode for the highest frequency and _n_per_ periods of the lowest
min_freq, max_freq = np.amin([mode_1T.frequency]), np.amax([mode_1T.frequency])
temporal_arr = np.linspace(0., n_per / min_freq, n_per * (max_freq / min_freq) * pt_per)

# Built Basis
MyBasis = sp.Basis(list_of_modes=[mode_1T])
MyBasis.print_self()  # Create pressure Signals

# Construct pressure signals
MyPressureSignals = sig.Pressure_signal(list_of_modes=[mode_1T], list_of_amplitudes=[10.], t_arr=temporal_arr)
MyPressureSignals.print_self()

import time

# time.sleep(1)

# Project pressure Signals on basis
Projected = pj.ProjectionOnBasis(Basis=MyBasis, Pressure_signals=MyPressureSignals, show_plot=False)
print Projected

list_amp = []

for mode_key in MyBasis.basis.keys():
    mode = MyBasis.basis[mode_key]
    amp = Projected.amps[mode]
    list_amp.append(amp)

    print 'shape amp', np.shape(amp)
    print 'amp = ', amp
    print 'type amp', type(amp)

    print "Plotting"

    pl.figure()
    pl.title("Complex amplitude for p_mode %s" % mode.name)
    pl.plot(np.real(amp), label='Re(' + mode.name + ')')
    pl.plot(np.abs(amp), label='abs(' + mode.name + ')')
    pl.plot(np.imag(amp), label='im(' + mode.name + ')')
    pl.legend()

if True:
    Reconstructed = sig.ReconstructedSignalFromBasis(name='RecTest', Basis=MyBasis, ProjectionOnBasis=Projected,
                                                     t_arr=temporal_arr)

    pl.title("Reconstructed")

# pl.show()
