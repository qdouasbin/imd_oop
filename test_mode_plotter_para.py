import numpy as np
import matplotlib.pyplot as pl
import multiprocessing

import spatial as sp
import Modes as md


if False:
    pl.style.use('my_bmh')
else:
    pl.style.use('MY_MATPLOTILB_STYLES/my_bmh_latex.mplstyle')
    #pl.style.use('MY_MATPLOTILB_STYLES/style_Q_Douasbin.mplstyle')

from geom_params import *

n_r = 30
n_th = n_r * int(3. * np.pi)
#n_th = 10.
n_z = 1

radius = np.linspace(0., 1., n_r, endpoint=True)
theta = np.linspace(0., 2. * np.pi, n_th, endpoint=False)

if n_z == 1:
    axial_coord = 0.

list_points_test = []

for i_r, r in enumerate(radius):
    for i_th, th in enumerate(theta):
        pt_name = 'r%s_th%s' % (i_r, i_th)
#       print "creating point %s" % pt_name
        list_points_test.append(sp.Point(name=pt_name, x=r*np.cos(th), y=r*np.sin(th), z=axial_coord))
        

# Creating set of points
set_plot_test = sp.Set_of_points(name='set_for_plot_test', list_points=list_points_test)
#Sop.print_self()

def para_plot(args):
    """
    Used to launch parallel plots.
    """
    my_m, my_n = args
    print "m, n", my_n, my_m
    mode_test_plot_1T = md.CylindricalModeTheo(name='1T_real', set_of_points=set_plot_test, m=my_m, n=my_n, q=0.)
    mode_test_plot_1T.print_self()
    md.plot_mode(mode_test_plot_1T, my_m, my_n)

# Multicore plot
pool = multiprocessing.Pool()
para_plot_args = zip([0,0,0,1,1,1,2,2,2], [0,1,2,0,1,2,0,1,2])
pool.map(para_plot, para_plot_args)


#pl.show()



