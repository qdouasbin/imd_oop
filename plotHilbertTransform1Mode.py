import numpy as np
import matplotlib.pyplot as pl
import os

import spatial as sp
import Modes as md
import Signals as sig
import Projection as pj

if True:
    pl.style.use('my_bmh')
else:
    pl.style.use('my_bmh_latex')

import matplotlib
matplotlib.rcParams['figure.figsize'] = (11., 6.)

os.system("rm -f Figures/Amplitudes/*pdf")
os.system("rm -f Figures/Analytic_signals/*pdf")
os.system("rm -f Figures/Original_signals/*pdf")
os.system("rm -f Figures/Reconstructed_signals/*pdf")
os.system("rm -f Figures/ValidationAmplitudeOneMode/*pdf")
os.system("mkdir Figures/ValidationAmplitudeOneMode/*pdf")

# ------------- Parameters --------------
lst_r = [1.]
# lst_th = np.array([0., 0.5, 1., 1.5]) * np.pi
# lst_th = np.linspace(0, 0.5, 2) * np.pi
lst_th = [0.]
lst_z = [0.]
# Creating temporal array
n_per = 3
pt_per = 200
# ------------- Parameters --------------


list_points_test = []

for i_r, r in enumerate(lst_r):
    for i_th, th in enumerate(lst_th):
        for i_z, zz in enumerate(lst_z):
            pt_name = 'r%s_th%s_z%s' % (i_r, i_th, i_z)
            list_points_test.append(sp.Point(name=pt_name, x=r * np.cos(th), y=r * np.sin(th), z=zz))

# Creating set of points
Sop = sp.Set_of_points(name='set_for_plot_test', list_points=list_points_test)

# Creating modes
mode_1T = md.CylindricalModeTheo(name='1T', set_of_points=Sop, m=0, n=1, q=0., user_frequency_spec=10.)

# Temporal array with _pt_per_ points per periode for the highest frequency and _n_per_ periods of the lowest
min_freq, max_freq = np.amin([mode_1T.frequency]), np.amax([mode_1T.frequency])
temporal_arr = np.linspace(0., n_per / min_freq, n_per * (max_freq / min_freq) * pt_per)

# Built Basis
MyBasis = sp.Basis(list_of_modes=[mode_1T])
MyBasis.print_self()  # Create pressure Signals

my_envelope = sig.env1(temporal_arr, t0=np.amax(temporal_arr / 2.5), delta=np.amax(
        temporal_arr) / 8.)  # + 0.02 * np.sin(2. * np.pi * mode_1T.frequency * temporal_arr / 5.)
# pl.xkcd()
# pl.figure()
# pl.plot(temporal_arr, my_envelope)
# pl.show()

# Construct pressure signals
MyPressureSignals = sig.Pressure_signal(list_of_modes=[mode_1T], list_of_amplitudes=[1.], #lst_enveloppe=[my_envelope],
                                        t_arr=temporal_arr)
MyPressureSignals.print_self()
MyPressureSignals.dump_plot_all_points()

if True:

    # Project pressure Signals on basis
    Projected = pj.ProjectionOnBasis(Basis=MyBasis, Pressure_signals=MyPressureSignals, show_plot=False)
    print Projected

    list_amp = []

    for mode_key in MyBasis.basis.keys():
        mode = MyBasis.basis[mode_key]
        amp = Projected.amps[mode]
        list_amp.append(amp)

        print 'shape amp', np.shape(amp)
        print 'amp = ', amp
        print 'type amp', type(amp)

        print "Plotting"

        pl.figure()
        # pl.title("Complex amplitude for p_mode %s" % p_mode.name)
        pl.plot(MyPressureSignals.time, np.real(amp), lw=2., label='$\Re(a_{' + mode.name + '})$')
        pl.plot(MyPressureSignals.time, np.imag(amp), '--', lw=2., label='$\Im(a_{' + mode.name + '})$')
        pl.plot(MyPressureSignals.time, np.abs(amp), ':', lw=3., label='$|a_{' + mode.name + '}|$')
        pl.xlabel('Time [s]')
        pl.ylabel('Amplitude [-]')
        pl.legend(mode="expand", borderaxespad=0, ncol=3)
        # pl.figlegend()
        pl.tight_layout()
        pl.savefig('./Figures/Amplitudes/Complex_amplitude_Mode_%s' % mode.name + '.png',
                   # transparent=True,
                   dpi=400)
        pl.close()

        pl.figure()
        # pl.title("Complex amplitude for p_mode %s" % p_mode.name)
        pl.plot(MyPressureSignals.time, 10. * my_envelope, '-o', lw=1, markevery=0.05, fillstyle='none',
                label='Enveloppe')
        pl.plot(MyPressureSignals.time[3:-3], np.abs(amp[3:-3]), '--', lw=2., label='$|a_{' + mode.name + '}|$')
        pl.xlabel('Time [s]')
        pl.ylabel('Amplitude [-]')
        pl.legend(loc='best')
        pl.tight_layout()
        pl.savefig('./Figures/Amplitudes/Env_vs_Complex_amplitude_Mode_%s' % mode.name + '.png',
                   # transparent=True,
                   dpi=400)
        pl.close()

    if True:
        Reconstructed = sig.ReconstructedSignalFromBasis(name='RecTest', Basis=MyBasis, ProjectionOnBasis=Projected,
                                                         t_arr=temporal_arr)
        # Intitial pressure dataframe
        points = MyPressureSignals.pressure_signal.keys()
        press_sig_1pt = MyPressureSignals.pressure_signal[points[0]]

        # reconstructed pressure dataframe
        mode = MyBasis.modes[0]
        amp_tmp = Projected.amps[mode]
        p_mode = np.array(mode)

        p_rec = Reconstructed.reconstructed_pressure_each_mode[mode]

        pl.close()
        pl.figure(figsize=(10, 4))
        pl.plot(MyPressureSignals.time, press_sig_1pt, '--', color='gray', lw=6.0, label='Measured pressure')
        pl.plot(MyPressureSignals.time, p_rec, '-', lw=2, fillstyle='none', markevery=0.05, label='Reconstructed')
        pl.xlabel('Time [s]')
        pl.ylabel('Pressure [Pa]')
        pl.legend(loc='best')
        pl.tight_layout()
        pl.savefig('./Figures/Reconstructed_signals/Reconstructed_vs_measured' + '.png',
                   # transparent=True,
                   dpi=400)
        pl.show()

