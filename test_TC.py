import os

import matplotlib.pyplot as pl
import numpy as np

import Modes as md
import Projection as pj
import Signals as sig
import spatial as sp

# getting template
# pl.style.use('my_bmh_latex')
pl.style.use('MY_MATPLOTILB_STYLES/style_Q_Douasbin.mplstyle')

# Cleaning output figures
os.system("rm -f Figures/Amplitudes/*pdf")
os.system("rm -f Figures/Analytic_signals/*pdf")
os.system("rm -f Figures/Original_signals/*pdf")
os.system("rm -f Figures/Reconstructed_signals/*pdf")

# Creating modes
mode_1T = md.AVSP_press_mode('INPUT_TC/MODES/basis_1T_TestCase.csv', name='1T')
print mode_1T
mode_1Tprime = md.AVSP_press_mode('INPUT_TC/MODES/basis_1Tprime_TestCase.csv', name="1T'")
print mode_1Tprime

# Building Basis
MyBasis = sp.Basis(list_of_modes=[mode_1T, mode_1Tprime])
MyBasis.print_self()  # Create pressure Signals

# Get pressure signal
hdf_file_signal = 'INPUT_TC/SIGNAL/RUN_AMT_1T_0_1_PHDREDAC.h5'
MyPressureSignals = sig.Pressure_signal_hdf(hdf_file_signal, list_of_modes=[mode_1T, mode_1Tprime])

# Get amplitude (projection of signals onto basis)
hdf_amplitude_output = 'OUTPUT_TC/AMPLITUDES/RUN_AMT_1T_0_1_PHDREDAC.h5'
Projected = pj.ProjectionOnBasis(Basis=MyBasis, Pressure_signals=MyPressureSignals, outfile_hdf=hdf_amplitude_output)
# MyPressureSignals.dump_plot_all_points()

# print Projected

list_amp = []

if False:
    pl.close()

    for mode_key in MyBasis.basis.keys():
        mode = MyBasis.basis[mode_key]
        amp = Projected.amps[mode]
        list_amp.append(amp)

    pl.figure()
    for amp in list_amp:
        # pl.title("Complex amplitude for p_mode %s" % p_mode.name)
        pl.plot(MyPressureSignals.time, np.real(amp), lw=2., label='$\Re(a_{' + mode.name + '})$')
        pl.plot(MyPressureSignals.time, np.imag(amp), ':', lw=2., label='$\Im(a_{' + mode.name + '})$')
        pl.plot(MyPressureSignals.time, np.abs(amp), '--', lw=3., label='$|a_{' + mode.name + '}|$')
        pl.xlabel('Time [s]')
        pl.ylabel('Amplitude [-]')
        pl.legend(loc='best')
        pl.tight_layout()
        # pl.savefig('./Figures/Amplitudes/Complex_amplitude_Mode_%s' % p_mode.name + '.png',
        # transparent=True,
        #           dpi=400)

Reconstructed = sig.ReconstructedSignalFromBasis(name='RecTest', Basis=MyBasis, ProjectionOnBasis=Projected)

# plot reconstructued vs signals
list_modes = MyBasis.modes

pl.figure()
for pt in mode_1T.pts[2:6]:
    md = list_modes[0]
    pl.title("Probe %s" % pt.name)
    pl.plot(MyPressureSignals.time, MyPressureSignals.pressure_signal[pt], label="%s" % pt.name)
    pl.legend()
    pl.tight_layout()
    pl.savefig('rec_probe%s' % pt.name)

for pt in mode_1T.pts:
    pl.figure()
    md = list_modes[0]
    pl.title("Probe %s" % pt.name)
    pl.plot(MyPressureSignals.time, MyPressureSignals.pressure_signal[pt], label="AVBP")
    pl.plot(MyPressureSignals.time, Reconstructed.rcst_p_sig[pt], 'o', fillstyle='none', label="All modes")
    pl.legend()
    pl.tight_layout()
    pl.savefig('rec_probe%s' % pt.name)

pl.show()
