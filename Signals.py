import matplotlib.pyplot as pl
import pandas as pd
from pylab import *
from scipy.special import *


class Pressure_signal_hdf(object):
    """
    Pressure signal object read from hdf data (readbin -> pandas -> hdf)
    """

    def __init__(self, hdf_file, list_of_modes=None):
        """
        Constructor of Pressure_signal
        """
        self.hdf_file = hdf_file
        self.dataframe = None
        self.list_of_modes = list_of_modes

        self.pressure_signal = {}
        self.time = None

        self.read_hdf()

    def read_hdf(self):
        """
    Read an hdf file from pandas:
    columns : time p_C1, p_C2,...,p_C8
    rows: t0, t1, t2....t_end
    :return:
    """
        df = pd.read_hdf(self.hdf_file)

        # Store initial dataframe
        length = len(df.index)
        # todo:debug
        df = df.iloc[0:1000]

        self.dataframe = df

        # Store temporal array
        self.time = df['time']

        # Store each signal in a dictionary (key = points)
        for pt in self.list_of_modes[0].pts:
            key_press = 'p_' + pt.name
            self.pressure_signal[pt] = df[key_press]

    def plot_self_1D(self, point, show_plot=False):
        pt_nm = point.name
        pt_x = point.x
        pt_y = point.y
        pt_z = point.z
        pt_th = point.th
        pt_r = point.r

        pl.figure()
        pl.plot(self.time, np.real(self.pressure_signal[point]), label=point.name)
        pl.xlabel("Time [s]")
        pl.ylabel("Pressure [Pa]")
        pl.tight_layout()
        pl.savefig('./Figures/Original_signals/Signal_Sythetic_pt_%s' % pt_nm + '.png',
                   # transparent=True,
                   dpi=400)
        if show_plot:
            pl.show()
        pl.close()

    def dump_plot_all_points(self):

        for pt in self.list_of_modes[0].pts:
            self.plot_self_1D(point=pt)  # plot, save but don't show


class Pressure_signal(object):
    """
    Analytic Pressure Signal using set of modes
    """

    def __init__(self, name='Default_pressure_signal_name', list_of_modes=None, list_of_amplitudes=None, t_arr=None,
                 lst_enveloppe=None):
        """
        Constructor of Pressure_signal
        """
        self.name = name
        self.list_of_modes = list_of_modes
        self.list_of_amplitudes = list_of_amplitudes
        self.lst_enveloppe = lst_enveloppe

        self.pressure_signal = {}
        self.time = t_arr

        self.compute_signals()

    def print_self(self):
        print "\n-----------------------------------------------------"
        print   "               Pressure Signal Class                                  "
        print   "-----------------------------------------------------"
        for i, mode in enumerate(self.list_of_modes):
            amp = self.list_of_amplitudes[i]
            print """Mode %s:  
            Amplitude           = %2.2e
            Max_modulus         = %2.2e
            Ratio Amp/Max_mod   = %2.2e""" % (mode.name, amp, mode.max_modulus, amp / mode.max_modulus)
        print "----------------------------------------------------\n"

    def compute_signals(self):
        print "Compute signal function activated"

        if self.time is None:
            print "t_arr not specified, set to 1. (Can be useful for reconstruction)"
            t_arr = 1.

        for pt in self.list_of_modes[0].pts:

            press_sig = 0. * self.time

            for i, mode in enumerate(self.list_of_modes):
                amp = self.list_of_amplitudes[i]

                if not self.lst_enveloppe is None:
                    amp = np.array(amp) * self.lst_enveloppe[i]
                    print "amp.shape -->", amp.shape
                    print "time.shape -->", self.time.shape
                    print "np.exp(1j * 2. * np.pi * p_mode.frequency * self.time)", np.shape(
                        np.exp(1j * 2. * np.pi * mode.frequency * self.time))

                press_sig += np.real(
                    np.array(amp) * np.exp(1j * 2. * np.pi * mode.frequency * self.time) * mode.mode[pt])
                print "max pressure of Mode %s = %2.2e" % (mode.name, np.amax(press_sig))

            # if self.lst_enveloppe is not None:
            #     press_sig = self.lst_enveloppe * press_sig

            self.pressure_signal[pt] = press_sig

        return None

    def plot_self_1D(self, point, show_plot=False):
        pt_nm = point.name
        pt_x = point.x
        pt_y = point.y
        pt_z = point.z
        pt_th = point.th
        pt_r = point.r

        pl.figure()
        pl.plot(self.time, np.real(self.pressure_signal[point]), label=point.name)
        pl.xlabel("Time [s]")
        pl.ylabel("Pressure [Pa]")
        pl.tight_layout()
        pl.savefig('./Figures/Original_signals/Signal_Sythetic_pt_%s' % pt_nm + '.png',
                   # transparent=True,
                   dpi=400)
        if show_plot:
            pl.show()

    def dump_plot_all_points(self):

        # Todo: make this parallel
        for pt in self.list_of_modes[0].pts:
            self.plot_self_1D(point=pt)  # plot, save but don't show


class ReconstructedSignalFromBasis(object):
    """
    """

    def __init__(self, name='Default_pressure_signal_name', Basis=None, ProjectionOnBasis=None):
        """
        Reconstruction of Pressure_signal
        """
        self.name = name
        self.Basis = Basis
        self.ProjectionOnBasis = ProjectionOnBasis

        # Reconstructed pressure signal
        self.rcst_p_sig_mode = {}
        self.rcst_p_sig = {}
        self.time = self.ProjectionOnBasis.Signals.time
        self.sum_reconstructed_pressure = np.zeros_like(self.time)

        self.reconstruct_signals()

    def reconstruct_signals(self):

        # Init output result
        for md in self.Basis.modes:
            self.rcst_p_sig_mode[md] = {}
            # print md
            for pt in md.pts:
                self.rcst_p_sig_mode[md][pt] = np.zeros_like(np.real(self.ProjectionOnBasis.amps[md]))
                self.rcst_p_sig[pt] = np.zeros_like(np.real(self.ProjectionOnBasis.amps[md]))

        # Loop over modes
        for md in self.Basis.modes:
            # print md
            for pt in md.pts:
                # print "pt -->", pt
                # print "md.p_mode[pt] -->", md.p_mode[pt]
                print "mode %s pt %s" % (md.name, pt.name)
                self.rcst_p_sig_mode[md][pt] += np.real(self.ProjectionOnBasis.amps[md] * md.p_mode[pt])
                self.rcst_p_sig[pt] += np.real(self.ProjectionOnBasis.amps[md] * md.p_mode[pt])

                #print "shapes rcst_p_sig_mode, rcst_p_sig", np.shape(self.rcst_p_sig_mode[md][pt]),             np.shape(self.rcst_p_sig[pt])

                #    # Loop over Points
                #    for pt in self.Basis.modes[0].p_mode.keys():
                #        print "pt -->", pt

                #        print "modes", self.Basis.modes
                #        print "type(modes)", type(self.Basis.modes)

                #        # Loop over modes
                #        pl.figure()
                #        for md in self.Basis.modes:
                #            print "md -->", md
                #            print md.p_mode[pt]
                #     print "md[pt]", md
                #     print "md[pt].p_mode", md[pt].p_mode

                #     pres_pt_md = md[pt].p_mode
                #     pl.plot(pres_pt_md, label='pt %s, md %s' % (pt.name, md.name))


                # # Initialization of press_sig
                # press_sig = np.empty(np.shape(self.time))

                # print "self.Basis.modes"
                # print "self.Basis.modes keys", self.Basis.modes.keys()
                # print "self.Basis.modes items", self.Basis.modes.items()

                # # Loop over modes
                # for i, p_mode in enumerate(self.Basis.modes):
                #     amp_tmp = self.ProjectionOnBasis.amps[p_mode]
                #     p_mode = np.array(p_mode.p_mode[pt])

                #     print "p_mode -->", p_mode

                #     print "np.shape(amp_tmp), np.shape(p_mode) -->"
                #     print np.shape(amp_tmp), np.shape(p_mode)

                #     # Todo: generalized this to n modes and n points!
                #     self.reconstructed_pressure_each_mode[p_mode] = np.real(amp_tmp * p_mode)

                #     pl.figure()
                #     pl.title("Reconstructed signal")
                #     pl.plot(np.real(amp_tmp * p_mode), label=p_mode.name)
                #     pl.legend(loc='best')
                #     pl.tight_layout()
                #     pl.savefig('./Figures/Reconstructed_signals/RecSig_Mode_%s_pt_%s' % (p_mode.name, pt.name) + '.png',
                #                # transparent=True,
                #                dpi=400)
                #     # pl.show()
        print "keys of rcst_p_sig_mode"
        for md_key in self.rcst_p_sig_mode.keys():
            for pt_key in self.rcst_p_sig_mode[md_key].keys():
                print md_key, pt_key


def env1(t, t0=None, delta=None):
    return np.tanh((t - t0) / delta) + 1.
