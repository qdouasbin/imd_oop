"""
Script containing the geometrical inputs
"""

# Length of cyl cavity
L = 1.

# Radius of cyl cavity
a = 2.

# SoundSpeed
c = 350.
