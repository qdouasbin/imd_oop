import numpy as np
import matplotlib.pyplot as pl
import os

def env1(t, t0=None, delta=None):
    return np.tanh((t - t0) / (delta)) + 1


if __name__ == "__main__":

    if False:
        pl.style.use('my_bmh')
    else:
        pl.style.use('my_bmh_latex')

    t_f = 20.  # final time

    t_arr = np.linspace(0., t_f, 100)



    pl.figure()
    # pl.plot([0., t_f], [1., 1.], '--', label='1')
    # pl.plot([0., t_f], [0., 0.], '--', label='0')
    pl.plot(t_arr, env(t_arr, t0=0, delta=t_f/4.), label='$\delta = %f$' % (t_f / 4.))

    # pl.xlabel('t/T')
    # pl.xlabel('Time')
    pl.legend()

    print "done"
    pl.show()
