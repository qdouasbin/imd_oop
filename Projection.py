import numpy as np
import pandas as pd
import scipy.signal as sgnl


class ProjectionOnBasis(object):
    def __init__(self, Basis=None, Pressure_signals=None, name='Default_projection_name', show_plot=False,
                 outfile_hdf='amp_proj_pandas.h5'):
        """
        Creates the Projection object that contains the contribution of each p_mode (in the input basis) for each timestep
        """
        self.Basis = Basis
        self.Signals = Pressure_signals
        self.name = name
        self.amps = {}
        self.show_plot = show_plot
        self.compute_amps()
        self.write_hdf_amp(outfile_hdf=outfile_hdf)

    def compute_amps(self):
        """
        Projects spatial arrays on temporal arrays
        """

        print '\n'
        for mode_key in self.Basis.basis.keys():
            md = self.Basis.basis[mode_key]
            print "Projection of signal onto p_mode %s" % md.name
            self.amps[md] = self.projector(md.p_mode)

    def projector(self, mode=None):
        """
        projection routine
        Get real_sig of shape (nb_points, len(time))
        """

        keys1, md = dict2np(mode)
        keys2, real_sig = dict2np(self.Signals.pressure_signal)

        analytic_signal = sgnl.hilbert(real_sig)  # Real dataframe to complex dataframe --> analytic signal

        amps_mode = []

        # number_points, number_time
        n_pt, n_t = analytic_signal.shape

        # Projection Loop --> Project at each time
        for i in xrange(n_t):
            s = analytic_signal[:, i]

            amps = np.vdot(md.flatten(), s)
            amps_mode.append(amps)

        return np.array(amps_mode)

    def write_hdf_amp(self, outfile_hdf):

        out_df = pd.DataFrame()

        for mode_key in self.Basis.basis.keys():
            md = self.Basis.basis[mode_key]
            out_df['a_' + md.name] = self.amps[md]

        store = pd.HDFStore(outfile_hdf)
        store['amplitudes_df'] = out_df
        store.close()


def __str__(self):
    out = "The projections have been perfomed.\nThe shape of the amplitude arrays are:"
    for k in self.amps.keys():
        print self.amps[k]
        print "shape amp %s" % k, np.shape(self.amps[k])
        # l, c = np.shape(self.amps[k])
        # out += "\t amplitudes %s\t:\t%s lines, %s columns" % (k, l, c)
    return out


def dict2np(dic):
    # arr = np.empty(shape=(1, len(dic)))

    keys = dic.keys()
    arr = np.array(dic.values())

    return keys, arr
