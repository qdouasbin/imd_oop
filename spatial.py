import numpy as np

class Point(object):
    """
    Point object
    """

    def __init__(self, name='Default_point_name', x=np.nan, y=np.nan, z=np.nan):
        """
        Creates point
        """
        self.name = name
        self.x = float(x)
        self.y = float(y)
        self.z = float(z)
        self.th = 0.
        self.r = 0.
        self.compute_theta()
        self.compute_r()

    def __str__(self):
        """
        Print all attributes
        """
        out = """
        -----------------------------------
        Point name  =   %s
        -----------------------------------
        Cartesian Coord. : 
            x Position      =   %+2.4e
            y Position      =   %+2.4e
            z Position      =   %+2.4e
        -----------------------------------
        Cylindrical Coord. : 
            r Position      =   %+2.4e
            theta Position  =   %+2.4e
            z Position      =   %+2.4e
        -----------------------------------
        """ % (self.name, self.x, self.y, self.z, self.r, self.th, self.z)
        return out

    def modify_coord(self, name=None, x=None, y=None, z=None):
        """
        Changes coord of point.name == name
        """
        self.x = x
        self.y = y
        self.z = z

    def compute_r(self):
        """
        Radial coordinate
        """
        x = self.x
        y = self.y
        self.r = np.sqrt(x**2. + y**2.)

    def compute_theta(self):
        """
        Radial coordinate
        """
        x = self.x
        y = self.y
        self.th = np.arctan2(y, x)


class Set_of_points(object):
    """
    Object containing a set of points
    """

    def __init__(self, name='Default_set_of_point', list_points=[]):
        """
        Creates a set of points form list of points
        :type list_points: list containing points
        """
        self.name = name
        self.points = list_points
        self.nb = len(list_points)

    def __add__(self, other):
        try:
            self.points.append(other)
            self.nb += 1
        except TypeError:
            print "You are not using type Points"

        return None

    def __str__(self):
        """
        Print list of all points
        """
        
        out = """----------------------------------------------------------------"""
        out += """\nThe set of points "%s" cointains %s points:\n""" % (self.name, self.nb)
        for i, pt in enumerate(self.points):
            #out += "Point number %s :" % (i + 1)
            out += pt.__str__()
        out += """----------------------------------------------------------------"""

        return out


class Basis(object):
    """
    Orthonormal basis based on Modes objects.
    """

    def __init__(self, basis_name='Default_basis', list_of_modes=None):
        """
        Creates a set of basis
        """
        if list_of_modes is None:
            list_of_modes = []
        self.name = basis_name
        self.modes = list_of_modes
        self.nb = len(list_of_modes)
        self.basis = {}
        self.fill_basis(list_of_modes)

    def fill_basis(self, list_of_modes):
        """
        Creates the basis = dictionary of modes
        """

        for i, mode in enumerate(self.modes):
            self.basis[i] = mode

    def __str__(self):

        out = """
-----------------------------------------------------
                        Basis
-----------------------------------------------------"""
        out += """
Basis name              = %s
Number of unit vectors  = %s
Family of vectors: """ % (self.name, self.nb)

        for i, mode in enumerate(self.modes):
            out += "\t\t\tVector %s --> %s" % (i + 1, mode.name)

        out += """
 -----------------------------------------------------"""
        return out

    def print_self(self):
        print self.__str__()


